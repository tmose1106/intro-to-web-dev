HTML
====

.. image:: _static/images/html5.svg
   :alt: the HTML 5 logo
   :align: center
   :width: 300px

HTML (`Hypertext Markup Language`_) is the standard used to organize web
page content on the World Wide Web. HTML consists of standardized text
elements saved within a *".html"* file.

HTML consists of *nested* elements which have parents and children.

Below is an example of `Hello World`_ in HTML followed by the rendered
representation.

.. code-block:: html
  :linenos:

  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="UTF-8">
      <title>This is a title</title>
    </head>
    <body>
      <!-- A paragraph element containing text -->
      <p>Hello, World!</p>
    </body>
  </html>

.. image:: _static/images/hello_world.png
  :alt: A simple hello world web page
  :align: center

Each part of the above sample will be discussed after some basic terminology.

.. _`Hello World`: https://en.wikipedia.org/wiki/%22Hello,_World!%22_program
.. _`Hypertext Markup Language`: https://en.wikipedia.org/wiki/HTML

Terminology
-----------

Element
  An HTML element is a node within an HTML document. Each element is
  denoted by one or two tags and possesses attributes to differentiate
  the element or to change the element's behavior.

Tag
  An HTML tag is the text representation of an HTML element. Most elements
  are represented using an opening and closing tag.

  An opening tag consists of a less-than sign, the tag name, and then a
  greater-than sign. The closing tag is very similar except it starts with a
  less-than sign followed by a forward slash. For example, ``<p>`` is an
  opening ``p`` tag, and ``</p>`` is a closing ``p`` tag.

  However, some elements consist of only one tag, and are denoted by a
  less-than sign, the tag name, a forward slash and then a greater-than sign.
  For example, ``<img >`` is a singular tag.

Attribute
  A key-value pair that describes an element.

  For example, ``<span class="label">Text</span>`` is a span tag with the
  ``class`` attribute set to the value of ``label``.

The Hello World Example
-----------------------

You may notice that there are a number of interesting tags in the Hello World
example above. We will walk through each line one by one.

Line 1: ``<!DOCTYPE html>``
  This tag is called the *"Doctype Declaration"*. It is only used at the top
  of an HTML document. It tells the web browser that this is, in fact, an HTML
  document.

Line 2: ``<html>``
  This tag defines the opening of the HTML content. Everything (other than
  the DOCTYPE declaration) must reside inside of these tags (on lines 2 and
  10 respectively). In other words, the ``<html>`` tag defines the root
  element of the page.

Line 3: ``<head>``
  The head tag contains metadata about the document. This data is for
  processing only and will not show anything on the web page itself.

  In this case, we only provide two data points: the character set and the
  title of the page.

Line 4: ``<meta charset="UTF-8">``
  This is a ``meta`` tag (short for *metadata*) and it contains some kind of
  information about the web page. Unlike the previous tags, this one is not
  only optional, but multipurpose.

  In this case we use the ``charset`` **attribute** to define the character
  set used by the document. Using the ``UTF-8`` character set is a reasonable
  default.

   .. note::

      If this line is not present, the web page will load but the browser
      may show a warning in the debugger (like below).

      .. image:: _static/images/encoding_error.png
         :alt: Firefox encoding error message
         :align: center

Line 5: ``<title>This is a title</title>``
  We use the ``title`` tags inside the ``head`` to set the title of the page
  to the text inside of the tags. This will set the name of the page on the
  tab, in history, for a bookmark, etc.

Line 6: ``</head>``
  This is the ending head tag.

Line 7: ``<body>``
  This tag denotes where the ``body`` of the document begins. Elements inside
  of the body will be rendered for the user to see.

Line 8: ``<!-- A paragraph element containing text -->``
  This line is a comment in HTML. It does not have any purpose or effect
  towards the page itself.

Line 9: ``<p>Hello, World!</p>``
  This line uses ``p`` tags (short for *paragraph*) to display a block of
  text. In this case, the text that will be displayed is ``Hello, World!``.

Line 10: ``</body>``
  This is the end of the body element.

Line 11: ``</html>``
  This is the end of the document.

.. important::

  Other than the tags inside of the ``body`` element, this example is a good
  `boilerplate`_ for a simple web page. One should attempt memorizing these
  elements and their purpose.

.. _`boilerplate`: https://en.wikipedia.org/wiki/Boilerplate_text

Common Body Elements
--------------------

There are a number of common elements HTML elements which can be found in a
document's body. An overwhelming majority of them default into either ``block``
or ``inline``.

``span``
  A generic *inline* element used for styling text

  .. note::
    Inline elements will be discussed later in `Display Types`_.

``a``
  An element which makes text into a clickable hyperlink. It is most commonly
  used to link to another web page, however it can also send the user to their
  desired email program (for an email address), a telephone app (for a phone
  number) or another part of the current web page. The destination is defined
  by the value of the ``href`` attribute.

  .. raw:: html

    <div class="jsfiddle-sample">
      <script async src="//jsfiddle.net/tmose1106/om4aezL3/embed/result,html/dark/"></script>
    </div>

``code``
  Used to display code. Typically it is shown by applying a ``monospace``
  font.

``em``
  An element which denotes emphasis on certain text. Typically browsers
  display this text using an *italicized* font.

  .. important::

    While there does exist an ``i`` tag which also italicizes font, this is
    note the point of HTML. Recall that HTML is made to organize semantic
    content and not to make said content look nice.

``strong``
  A inline element which denotes important text. Typically it is shown using
  **bold** font.

``button``
  A general buttom elements which is generally made to be clicked on.

  .. raw:: html

    <div class="jsfiddle-sample">
      <script async src="//jsfiddle.net/tmose1106/gzj1amf2/embed/result,html/dark/"></script>
    </div>

``div``
  A general container for items. However, browsers now support more specific
  container tags, like ``main``, ``article``, ``section``, ``header``,
  ``footer``, ``nav``, etc.

  .. note::
    Inline elements will be discussed later in `Display Types`_.

``h1``, ``h2``, ..., ``h6``
  Header elements of varying levels which are used to denote sections and
  subsections.

  .. raw:: html

    <div class="jsfiddle-sample">
      <script async src="//jsfiddle.net/tmose1106/kacyeo2t/embed/result,html/dark/"></script>
    </div>

``p``
  A block of text which represents paragraph. It ignores the formatting of
  the text inside.

``pre``
  A block of text which is preformatted (includes multiple lines, leading spaces, etc.)

  .. raw:: html

    <div class="jsfiddle-sample">
      <script async src="//jsfiddle.net/tmose1106/s6yL4wcu/embed/result,html/dark/"></script>
    </div>

``li``
  An item to be placed within a list element (``ul``, ``ol``, etc.)

``ul``
  An unordered (bulleted) list of items

  .. raw:: html

    <div class="jsfiddle-sample">
      <script async src="//jsfiddle.net/tmose1106/fukg8epd/embed/result,html/dark/"></script>
    </div>

``ol``
  An ordered (numbered) list of items

  .. raw:: html

    <div class="jsfiddle-sample">
      <script async src="//jsfiddle.net/tmose1106/zq1aebnk/embed/result,html/dark/"></script>
    </div>

``img``
  Inserts an image into the text. The image itself is *fetched* from the URL
  defined by the ``src`` attribute. If the image cannot be found, it is
  replaced by the text set by the ``alt`` attribute.

  .. raw:: html

    <div class="jsfiddle-sample">
      <script async src="//jsfiddle.net/tmose1106/kw67fy0n/embed/result,html/dark/"></script>
    </div>

  .. note::

    One may wonder why images can be resized but are also considered ``inline``
    elements by most browsers. You can read this terrific article on the
    topic of `replaced elements`_.

    Notice that this is a higher level topic dealing with the intracies of
    rendering HTML and is not necessary to produce HTML.

``input``
  An element which represents a place for user input in a form. The type of
  input is defined by the element's type attribute. Most of these are simple
  inline elements, however some types (``color``, ``date``, ``file``) have
  more functionality than you might expect.

  The below example shows many of the ``input`` types.

  .. raw:: html

    <div class="jsfiddle-sample">
      <script async src="//jsfiddle.net/tmose1106/qs49L6bx/embed/result,html/dark/"></script>
    </div>

.. _`replaced elements`: https://www.sitepoint.com/replaced-elements-html-myths-realities/

``table``
  The ``table`` element is a special block-like element for display tables of
  data. The contain special elements specific to tables.

  ``thead``
    A container for table header elements.

  ``tbody``
    A container for rows of table data.

  ``tr``
    A row inside of the table.

  ``th``
    An element containing a column heading.

  ``td``
    An element containing a data value.

  .. raw:: html

    <div class="jsfiddle-sample">
      <script async src="//jsfiddle.net/tmose1106/rw628a1p/embed/result,html/dark/"></script>
    </div>

  .. note::

    At this point, our table looks pretty useless and "ugly". Also, many of
    these internal table elements might seem redundant. Both of these gripes
    will remedied after we discuss styling elements with CSS.

.. _`Display Types`: ./display_types.html

Special Attributes
------------------

As mentioned previously, attributes are key-value pairs applied to elements.
Some attribute names have special uses. Some are specific to certain elements
(i.e. ``href`` on ``a`` elements) and produce a desired effect. However, some
attributes can be applied to any element.

``class``
  The ``class`` attribute is used to differentiate a set of elements with a
  common purpose. Many elements can have the same class. Also, an element can
  have multiple classes (separated by spaces).

  .. code-block:: html

    <p class="warning">This is your first warning!</p>
    <p class="warning important">This is your last warning!</p>

``id``
  An attribute used to differentiate an element from all others in the
  document. An ID should be unique to an element. As such, only one element
  should possess an ID.

  .. code-block:: html

    <p id="super-important">READ THIS PLEASE</p>

These special attributes will be used to select elements from the document
using CSS and JavaScript.

Helpful Technologies
--------------------

There are a number of libraries and programs available which can ease the
burden of writing of HTML by hand when a large level of flexibility is not
needed.

While most of these technologies will help improve development speed in some
cases, the will require some kind of programming on the developers part to
get set up.

Markdown
++++++++

Markdown is a simplified standard for writing HTML and other structured
documents. It allows the user or developer to write content more easily
without worrying about which HTML tag to use.

For example, below is an example Markdown document containing a heading
and a paragraph containing a hyperlink. Below that is the HTML that is
produced after processing (by a parser like `cmark`_).

.. code-block:: md
  :linenos:

  # Hello, World

  This is an example with a [hyperlink](https://daringfireball.net/projects/markdown/).

.. code-block:: html
  :linenos:

  <h1>Hello, World</h1>
  <p>This is an example with a <a href="https://daringfireball.net/projects/markdown/">hyperlink</a>.</p>

Unfortunately, Markdown does not have a syntax for every HTML tag, but it
does provided enough to write a blog post or simple article. You can learn
more about Markdown syntax on the `commonmark`_ website.

.. _`commonmark`: https://commonmark.org/help/
.. _`cmark`: https://github.com/commonmark/cmark

Templates
+++++++++

Creating HTML documents using templates allows one to "drop" data into a
preformatted HTML document.

There are many templating languages written in a variety of programming
languages. One example of a simple templating language is `mustache`_.
For a document to be a mustache template, the only requirement is that said
document contains a value name surrounded by ``{{`` and ``}}`` characters.
This tells mustache that this is where we want to place a value.

Using `chevron`_, a Python implimentation of mustache, we can easily render
mustache templates. Below is an example where we have a small HTML document
stored inside of a multiline string.

.. code-block:: python3
  :linenos:

  from chevron import render

  document = """<!DOCTYPE html>
  <html>
    <head>
      <meta charset="UTF-8">
      <title>Personal Description: {{ name }}</title>
    </head>
    <body>
      <p>Hello, my name is {{ name }}!</p>
    </body>
  </html>"""

  rendered_document = render(document, {"name": "Alice"})

  print(rendered_document)

The output of the above script would be:

.. code-block:: html
  :linenos:

  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="UTF-8">
      <title>Personal Description: Alice</title>
    </head>
    <body>
      <p>Hello, my name is Alice!</p>
    </body>
  </html>

Notice how every occurence of ``{{ name }}`` has been replaced with the
provided named of *Alice*.

.. note::

  Templates like these are not specific to HTML. Templating can be used
  in any situation where data must be applied to a string or text file.
  Writing web pages just happens to be a great example.

.. _`chevron`: https://github.com/noahmorrison/chevron
.. _`mustache`: https://mustache.github.io/
