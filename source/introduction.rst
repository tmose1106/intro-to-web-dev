Introduction
============

This workshop site intends to provide students with a concise introduction to
web development technologies through examples. To begin, a focus will be
placed upon each of the three technologies that compose the World Wide Web.
Afterwards, students will be provided with real-world examples which utilize
these technologies to serve a specific purpose.

Why Web Development?
--------------------

The importance of the web increases with every day. In our modern day, people
interact with the World Wide Web on a regular basis.  Using modern web
technologies we can create anything as simple as a blog to a full ecommerce
application and beyond. With the arrival of powerful mobile devices, today's
need for flexible, mobile-ready web applications is higher than ever.

Frontend vs. Backend
--------------------

When it comes to web development, there are two major fields:

Frontend
  Designing the appearence of the website and the user experience using web
  technolgies.

Backend
  Designing the systems which provide users with data, the ability to
  authenticate, etc. using any programming language.

Below is a visual depiction of the stereotype surrounding frontend vs. backend
development (sourced from a `reddit post`_).

.. image:: _static/images/frontend_vs_backend.jpg
  :alt: A humorous depiction of frontend vs. backend
  :align: center
  :width: 400px

For this reason, the workshop will focus heavily on the *frontend* aspect of
web development. However, if you are interested in backend development, having
a good understanding of the frontend technologies is a great start.

.. _`reddit post`: https://www.reddit.com/r/ProgrammerHumor/comments/7zfgwg/frontend_vs_backend/

Prerequisites
-------------

There are no major prerequisites for this workshop other than a web browser
(Firefox, Chrome, etc.) and a text editor. As all of the technologies we will
be discussing are based on plain text, you can use any text editor.

.. hint::

  Despite being able to use any text editor for this workshop, I would suggest
  finding an editor which supports plugins. This is so you can install a
  plugin which assist with typing HTML. As you will soon find it, it can be
  somewhat annoying to type by hand.

Samples
-------

Throughout this website, there are a number of rendered examples. These
examples are hosted on `jsfiddle.net`_, a playground for frontend web development.
I would suggest everyone to try it out while learning to see how elements act,
how styles apply, etc.

.. _`jsfiddle.net`: https://jsfiddle.net/
