Welcome to Intro to Web Dev!
============================

The purpose of this site is to provided a concise gateway into the
technologies used by the `World Wide Web`_: HTML, CSS and JavaScript.
Afterwards, students are provided with demos which make use of best
practices regarding these technologies.

.. toctree::
  :maxdepth: 1
  :hidden:

  introduction
  html
  css
  display_types
  javascript
  canvas
  demos

.. _`World Wide Web`: https://en.wikipedia.org/wiki/World_Wide_Web

About This Workshop
===================

This site is being developed for the Rutgers University `Novice to Expert
Coding Club`_ (N2E) Intro to Web Development workshop. It is intended to act
as "slides" to aid in teaching basic web dev at a one-time two hour lecture
workshop hosted by N2E.

============= =========================================
Live Website  `tmose1106.gitlab.io/intro-to-web-dev`_
Source Code   `gitlab.com/tmose1106/intro-to-web-dev`_
============= =========================================

.. _`Novice to Expert Coding Club`: http://n2ecodingclub.rutgers.edu/
.. _`tmose1106.gitlab.io/intro-to-web-dev`: https://tmose1106.gitlab.io/intro-to-web-dev/index.html
.. _`gitlab.com/tmose1106/intro-to-web-dev`: https://gitlab.com/tmose1106/intro-to-web-dev

Licensing
=========

This project is released under the terms of the `Creative Commons Attribution
4.0 International`_ license. For more information, read the *LICENSE.txt*.

.. _`Creative Commons Attribution 4.0 International`: http://creativecommons.org/licenses/by/4.0/
