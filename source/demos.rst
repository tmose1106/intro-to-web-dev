Demos
=====

This section provides links several real-world examples of using modern HTML,
CSS and JavaScript standards. The source for these projects can be found in
`this repo`_.

============== ======================================================
Name           Live Demo                                             
============== ======================================================
Calendar       `tmose1106.gitlab.io/web-demos/calendar`_             
Dodge Game     `tmose1106.gitlab.io/web-demos/canvas/dodge.html`_    
Platform Game  `tmose1106.gitlab.io/web-demos/canvas/platform.html`_ 
Shows Grid     `tmose1106.gitlab.io/web-demos/shows`_                
============== ======================================================

.. _`this repo`: https://gitlab.com/tmose1106/web-demo-shows
.. _`tmose1106.gitlab.io/web-demos/calendar`: https://tmose1106.gitlab.io/web-demos/calendar/index.html
.. _`tmose1106.gitlab.io/web-demos/canvas/dodge.html`: https://tmose1106.gitlab.io/web-demos/canvas/dodge.html
.. _`tmose1106.gitlab.io/web-demos/canvas/platform.html`: https://tmose1106.gitlab.io/web-demos/canvas/platform.html
.. _`tmose1106.gitlab.io/web-demos/shows`: https://tmose1106.gitlab.io/web-demos/shows/index.html