CSS
===

.. image:: _static/images/css3.svg
   :alt: the CSS 3 logo
   :align: center
   :width: 200px

CSS (`Cascading Style Sheets`_) is the World Wide Web standard for describing how
HTML elements will be rendered.

Below is an example of CSS within an HTML document:

.. raw:: html

  <div class="jsfiddle-sample">
    <script async src="//jsfiddle.net/tmose1106/L2dyjwe7/embed/result,html,css/dark/"></script>
  </div>

.. hint::

  Comments in CSS use C style comments which begin with ``/*`` and end with
  ``*/``.

.. _`Cascading Style Sheets`: https://en.wikipedia.org/wiki/Cascading_Style_Sheets

Terminology
-----------

Selector
  A name consisting of an HTML tag name, element ID, class or attribute used
  to *pick* an HTML element out of a document for styling.

Declaration
  A line used to change a setting of an element. Every declaration consists
  of a declaration name, a colon, a value and a semicolon (i.e. ``color:
  red;``). Multiple declarations are contained inside of a declaration block.

Specificity
  The order used to determine which styles will apply to an element.

  .. hint::

    I found this excellent video on specificity where they explain pretty much
    everything you need to know. `Watch it here`_.

.. _`Watch it here`: https://www.youtube.com/watch?v=c0kfcP_nD9E

Specifying Elements
-------------------

Notice that CSS is composed of selectors which "pick out and then
declarations which change some *settings* of the selected element.

As such, CSS provides many ways to specify or "pick out" an element so that
it may be styled.

Element Selector
  A selector consisting of an element type. This will select all elements of
  that type.

  In the example below, all ``span`` elements in the document are selected.

  .. code-block:: css

    span { color: red; }

Class Selector
  A selector using the ``.`` operator followed by a class name. It will
  select all elements with that class name in their ``class`` attribute.

  In the example below, all elements with the class of *warning* are
  selected.

  .. code-block:: css

    .warning { color: red; }

ID Selector
  A selector using the ``#`` operator followed by an ID name. It will
  select the element with this ID name in its ``id`` attribute.

  Below, the element with the ID of *warning* is selected.

  .. code-block:: css

    #warning { color: red; }

These types of selectors can be compounded. For example, all ``span`` elements
with the class of *warning* and *active* can be selected using the following
CSS:

.. code-block:: css

  span.warning.active { color: red; }

Furthermore, selectors can be compounded to select elements inside of other
elements. Building upon the previous example, we can select ``span`` elements
with the class *active* nested inside of ``div`` elements with the ID
``message``.

.. code-block:: css

  div#message span.active { color: red; }

Finally, selectors can be grouped by separating them with a comma to apply a
common set of declarations. In this example, we apply a style to ``span``
elements with the class *error* or *warning*.

.. code-block:: css

  span.error, span.warning { color: red; }

Defining Colors
---------------

A number of CSS declarations accept colors as values. CSS provides a number
of ways to define a colors.

Predefined Keywords
  There is a `large set of predefined colors`_ which can be accessed by name.
  For example, *blue*, *lightblue*, *darkblue*, *deepskyblue* and *aliceblue*
  are all valid keywords.

Hexadecimal Codes
  Most commonly, colors can be defined using hexadecimal codes. For example,
  *#0000ff*, *#a9a9a9* and *#fafad2* (blue, darkgrey and lightgoldenrodyellow
  respectively) are all valid color codes.

  Hexadecimal codes can be extended two more places to add a transparency
  value.

RGB Values
  RGB values are essentially the same as hexidecimal values, but are written
  in base 10 integers inside of the RGB function.

  Similarly, there is an RGBA function which takes four arguments: three
  integers (for RGB) and a float (between 0 and 1) for percent transparency.

  .. raw:: html

    <div class="jsfiddle-sample">
      <script async src="//jsfiddle.net/tmose1106/fs2jt081/embed/result,html,css/dark/"></script>
    </div>

.. _`large set of predefined colors`: https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#Color_keywords

Units of Measure
----------------

Another important feature of CSS is the units it provides. These units are
used to give block objects a size. These units may be absolute (the same on
every display) or relative (defined by the size of the display.

px (pixel)
  A single pixel.

pt (point)
  A single point (1/72 of an inch)

% (Percent)
  Each percent unit is based off the width of the element's parent.

vh (View Height)
  A percent of the total height provided by the screen.

vw (View Width)
  A percent of the total width provided by the screen.

ch
  The width of the *0* character in the current font.

em
  Current font size of the element.

rem
  Current font size of the ``root`` element.

lh
  Height of a line of text

rlh
  Height of a line of text in the root element

Applying to HTML
----------------

There are two ways to apply CSS to an HTML document:

* Using a ``link`` tag in the document ``head``
* Including CSS between ``style`` tags inside the HTML document

``link`` Tag
++++++++++++

To apply using a link tag, include a line like this:

.. code-block:: html
  :linenos:

  <link rel="stylesheet" type="text/css" href="theme.css">

``style`` Tags
++++++++++++++

Alternatively, one can include the CSS directly using the ``style`` tags:

.. code-block:: css
  :linenos:

  <style>
    p { font-size: 2em; }
  </style>

Helpful Technologies
--------------------

Sass
++++

`Sass`_ (Syntactically Awesome Style Sheets) is a CSS preprocessor which
makes writing complex stylesheets easier by adding new features.

For starters, Sass allows one to remove curly braces and semicolons by
using indentation to define a block (similar to programming languages like
Python).

.. code-block:: sass

  span.warning
    color: red
    background-color: yellow

.. code-block:: css

   span.warning {
     color: red;
     background-color: yellow;
   }

You may also nest styles to make stronger specificity easier to achieve.
The example below may not seem like a big deal, but when there are hundreds
of declarations, it makes a world of difference.

.. code-block:: sass

  p.warning
    border: solid red

    span.highlight
      color: red
      background-color: yellow

.. code-block:: css

   p.warning {
     border: solid red;
   }

   p.warning span.highlight {
     color: red;
     background-color: yellow;
   }

Sass also provides variables which can be reused for repetitive tasks like
apply a common color across multiple elements.

.. code-block:: sass

  $background-color: rgb(100, 60, 180)
  $foreground-color: rgb(224, 224, 244)

  header, footer
    background-color: $background-color
    color: $foreground-color

.. code-block:: css

  header, footer {
    background-color: #643cb4;
    color: #e0e0f4;
  }

As you can see, Sass provides many useful tools which make writing
style sheets less of a chore. There are many more features than above, so
check out their `beginners guide`_ to learn more.

Bootstrap
+++++++++

Bootstrap is a framework for creating mobile-ready websites using premade
themes and components. Bootstrap themes are composed of selectors with
standardized names, allowing a web developer to make create a unique looking
website without worry about CSS. Thus it is heavily used in prototyping.

`Bootswatch`_ is a collection of Bootstrap themes which are completely free.
Alternatively, there are `Bootstrap themes` which can be purchased on the
offical website.

.. _`beginners guide`: https://sass-lang.com/guide
.. _`Bootstrap themes`: https://themes.getbootstrap.com/
.. _`Bootswatch`: https://bootswatch.com/
.. _`Sass`: https://sass-lang.com/
