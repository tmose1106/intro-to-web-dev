Display Types
=============

Elements can be displayed on a page many different ways. 

.. hint::

  If you wish to explore this topic further, I would suggest watching this
  `incredibly concise video`_ on display properties.

.. _`incredibly concise video`: https://www.youtube.com/watch?v=dL-_uVvbMU8

Hiding Elements
---------------

A common value is the ``none`` display type. Essentially, this will
make it seem that the element is not part of the page, as it will not
displayed at all.

Default Display Types
---------------------

When it comes to default style values, elements typically fall into one
of these three display types:

inline
    An element which fits in a line of text and changes the behavior of a piece
    of text. It is only as large as the text it contains.

    Some examples include ``span`` (the generic inline element), ``a``,
    ``em``, ``strong``, etc. which fit inside of a line of text.

block
    An element on its own line that (by default) takes up the entire width of
    its parent element. These elements can have their width and height changed
    manually.

    Some examples include ``div`` (the generic block element), ``p``, ``ul``,
    and many more which reside on their own line.

inline-block
    The item fits inline with text, but acts as a block (can have a width,
    height, etc).

Below is an example of how each of these display types work:

.. raw:: html

  <div class="jsfiddle-sample">
    <script async src="//jsfiddle.net/tmose1106/qba5tn0o/24/embed/result,html,css/dark/"></script>
  </div>

Responsive Display Types
------------------------

With the rise of mobile web browsing, responsive web development has become
a necessity. This has led to web standards developing more advanced display
types. Beyond being more responsive, these make defining complex layouts much
easier for web developers.

flex
  Allows creating responsive column or row layouts. It works with a series of
  other CSS properties to configure the element further.

  In the below example, we use the ``flex-grow`` property on the child
  elements. This property allows an element to have variable dimensions

  .. raw:: html

    <div class="jsfiddle-sample">
      <script async src="//jsfiddle.net/tmose1106/fkr9dqm3/embed/result,html,css/dark/"></script>
    </div>

grid
  Defines a number of rows and columns for children elements to be layed out.
  Similar to ``flex``, there are many CSS properties for the element itself
  and its children to modify their behavior.

  .. raw:: html

    <div class="jsfiddle-sample">
      <script async src="//jsfiddle.net/tmose1106/39b6tfzw/embed/result,html,css/dark/"></script>
    </div>