JavaScript
==========

.. image:: _static/images/javascript.svg
  :alt: Javascript logo
  :align: center
  :width: 300px

`JavaScript`_ is the scripting language of the World Wide Web. When used
in conjunction with HTML and CSS, it allows for web sites to become
interactive through scripting.

Unlike HTML and CSS, JavaScript is in fact a full fledged programming
language. This section will focus moreso on JavaScript's web features rather
than all of JavaScript's available syntax.

Quick Syntax Tutorial
---------------------

As a simple hello world example, we can send a simple debug message to our
browser using the ``console.log`` function. In the example below you will also
notice that semicolons are required after statements.

.. code-block:: js

  console.log("Hello, World!");

Alternatively, we can warn the user with a message using ``window.alert``.

.. code-block:: js

  window.alert("Hello, World!");

There are two ways to define a variable in modern JavaScript:

  * ``let`` assigns a mutable variable
  * ``const`` assigns a immutable variable

.. code-block:: js
  :linenos:

  let basic_var = 0;        // This value can be changed
  const immutable_var = 86; // This value cannot

.. note::

  There is actually a third way: the ``var`` statement. In the past, this was
  the only way to define variables, however its behavior is somewhat confusing
  relative to assigning variables in other languages. In most cases, ``let``
  and ``const`` make more sense.

JavaScript also includes all of the basic control statements one would expect
of a programming language.

.. code-block:: js
  :linenos:

  const greet = true;

  if (greet)
    console.log("Hello there!");
  else
    console.log("There is nothing to be said.");

  let str = "";

  for (let i = 0; i < 9; i++) {
    str = str + i;
  }

One surprising feature is that functions in JavaScript are typically saved
to variables like below.

.. code-block:: js
  :linenos:

  let my_func = function(arg1, arg2) {
    return arg1 + arg2;
  };

These are the only syntax elements that one needs to know for the remainder of
this section.

Working with Objects
--------------------

At it's base, JavaScript is a scripting language which makes heavy use of
`object oriented programming`_. However, knowledge of how objects are design
is not required for beginning. All one really need to know is:

* An object is a pragmatic way of representing a *thing*. In the case of
  JavaScript, we will be working with objects like ``document``, ``window``,
  and ``console``. Each of these represents a respective *thing*.
* Objects possess **attributes**, which are essentially variables which define
  the state of an object.
* Objects also possess **methods**, which are functions that act up internal
  data (AKA attributes).
* Objects that represent elements have specially named methods which are ran
  when the user performs some kind of action. These methods are called
  **callbacks**.

A majority of the operations we will be performing in this tutorial will be
using the `document object`_. This object represents the open web page: it
provides data about the page (the URL, the ``body`` element text, etc.) and
methods which allow us to modify the page (add new elements, modify existing
elements, etc.)

.. _`document object`: https://developer.mozilla.org/en-US/docs/Web/API/Document
.. _`object oriented programming`: https://en.wikipedia.org/wiki/Object-oriented_programming

Selecting Elements
------------------

Similar to CSS, a major part of working with JavaScript is selecting
elements using element names, classes and ID values. The document object
provides a number of methods for selecting elements.

``getElementById()``
  Return the element with the provided ID value.

``getElementsByClassName()``
  Return a list of elements with the provided class name value.

``getElementsByTagName()``
  Return a list of elements of the provided element type.

``querySelector()``
  Return the first element that matches the query.

  .. note::

    A query is a combination of element, class and ID like the selectors in
    CSS.

``querySelectorAll()``
  Return a a list of elements that match the query.

.. note::

  All of the provided methods are describe in the `documentation for the
  document object`_.

Using the ``getElementById`` method in conjunction with HTML and CSS, one can
create a simple dark mode for any web page.

.. raw:: html

  <div class="jsfiddle-sample">
    <script async src="//jsfiddle.net/tmose1106/8u04osa1/embed/result,html,css,js/dark/"></script>
  </div>

.. _`documentation for the document object`: https://developer.mozilla.org/en-US/docs/Web/API/Document#Methods

Creating New Elements
---------------------

Unsurprisingly, one can create elements at runtime. The process of doing so
may seem convoluted at first, so let's break it down into steps:

* Create an element using the ``document.createElement`` method
* Create a text node using the ``document.createTextNode`` method
* Add elements and nodes using the ``appendChild`` method of each element
  object

Below is an example where we add someone's name and phone number to a contacts
table.

.. raw:: html

  <div class="jsfiddle-sample">
    <script async src="//jsfiddle.net/tmose1106/35ou4zq8/embed/result,html,css,js/dark/"></script>
  </div>

Styling Using JavaScript
------------------------

It is possible to style elements using JavaScript as well. While this does not
supersede CSS in any way, it is helpful for when dynamic styling is necessary.
This is completed by selecting an element, saving it, and then using the
``style`` attribute. The style attribute references a style object belonging
to the element. This object provides most of the same CSS styles under very
similar names. However, while css uses dashed between works, this object use
camel case (i.e. ``background-color`` in CSS is ``backgroundImage`` in
JavaScript.

.. raw:: html

  <div class="jsfiddle-sample">
    <script async src="//jsfiddle.net/tmose1106/jfvmytu3/embed/result,html,css,js/dark/"></script>
  </div>

.. _`style object`: https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/style

Linking with HTML
-----------------

Continuing the parallels with CSS, linking a JavaScript file (*.js* extension)
with an HTML file requires either embedding the code or linking to a file
using a relative or external URL. However, this time, there is only one tag
to do so: the ``script`` tag.

Embedding
---------

.. code-block:: html
  :linenos:

  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="UTF-8">
      <title>This is a title</title>
    </head>
    <body>
      <p id="main">Hello, World!</p>
      <!-- Include the script inline -->
      <script>
        let main_paragraph = document.getElementById("main");
      </script>
    </body>
  </html>

Linking
-------

.. code-block:: html
  :linenos:

  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="UTF-8">
      <title>This is a title</title>
    </head>
    <body>
      <p>Hello, World!</p>
      <!-- Link to a local file called "main.js" -->
      <script src="./main.js"></script>
    </body>
  </html>

Adding Event Listeners
----------------------

A very important concept in JavaScript is **callbacks**. A callback is a
function we write which is called at later time. This is great for the web,
where there are many different times we might want to run a function when
the user performs and action.

A good example of this is adding a click callback. Using JavaScript, we can
add specific callbacks using the `addEventListener()`_ method of the
element. The first argument is the name of the event we would like to handle,
and the second is the function we would like to call.

.. raw:: html

  <div class="jsfiddle-sample">
    <script async src="//jsfiddle.net/tmose1106/fstjwoea/embed/result,html,js/dark/"></script>
  </div>

.. hint::
  
  In the example above, we use an `arrow function`_. Essentially, it is a
  function *which doesn't create a new scope*. This is a bit outside
  the scope of this tutorial, but feel free to research further!

.. note::

  You may see the event handler ``.onclick``, which is the older method of
  add event listeners.

.. _`addEventListener()`: https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener
.. _`arrow function`: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions