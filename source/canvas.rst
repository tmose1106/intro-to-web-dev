Canvas Element
==============

The HTML ``canvas`` element is a block level element which allows rendering
arbitrary pixels, shapes, and animations using JavaScript. This allows for
the creation of custom applications on the web. The ``canvas`` is very
popular for developing web-based games, since it was designed to be a
replacement for `Adobe Flash`_ graphics.

.. _`Adobe Flash`: https://en.wikipedia.org/wiki/Adobe_Flash

Anatomy of the Canvas
---------------------

There are two main parts to the canvas:

* The canvas element, which is a HTML element and define's its placement in
  the document.
* The context is the JavaScript object which has methods for drawing.

Below is a simple example of drawing two different color rectangles on a
canvas. In the HTML, notice that the ``height`` and ``width`` attributes.
These define the size of the **context**, not the canvas element.

.. raw:: html

  <div class="jsfiddle-sample">
    <script async src="//jsfiddle.net/tmose1106/x1phd2yf/embed/result,html,css,js/dark/"></script>
  </div>
